# README #

1. ``git clone https://naumangla@bitbucket.org/naumangla/naumancart.git``
2. ``cd /naumancart``
3. Install local-web-server dependency: ``npm install --save-dev`` (requires node v4.0.0 or higher)
4. ``npm start`` will start the server and return the URLs (ie ``http://127.0.0.1:8100``)

Note: I only used node to run the server and used CodeKit to compile SCSS and JS. If you have access to CodeKit, I left the config file in the repo.