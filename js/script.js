// !-------- Cart
// -----------------------------------------------------------------------------

var totalCost = 0;

$(document).on('click', '.buy', function(event) { 
    
    // !-------- Animate Cart Icon
    
    $('#cartIcon').addClass('updated');
    
    setTimeout(function(){
        $('#cartIcon').removeClass('updated');
    }, 500);
    
    // !-------- Update Total Price
    
    var itemCost = $(this).attr('data-price');
    
    totalCost = parseFloat(totalCost)+parseFloat(itemCost);
    
    if ( totalCost > 0 ) {
        $('#cartTotal').text('$'+totalCost).show();
        $('#cartSum span').text('$'+totalCost);
    }
    
    // !-------- Add to Cart Listing
    
    var cartHtml = '<li data-sku="'+$(this).attr('data-sku')+'" data-cost="'+itemCost+'">'+$(this).attr('data-name')+'<span class="price">$'+itemCost+'</span></li>'
    
    $('#yourCart .cartItems').append(cartHtml);
});

// !-------- Cart Toggle
// -----------------------------------------------------------------------------

$('#cartIcon, #cartTotal').click(function() {
    
    $('.cartWrapper').toggleClass('open');
    
});























